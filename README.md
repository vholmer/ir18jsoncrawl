# README #
Extract the songlyrics.zip file so that songdata.csv is in the root directory.

Then, simply run crawl.py to collect all the json data.

(Make sure that there is a directory called 'json' in the root)

### Installing Dependencies

To install the required dependencies, run:

pipX.Y install -r requirements

Where X.Y is your version number (optional if your pip alias is already correct version) e.g. pip3.6 etc.
