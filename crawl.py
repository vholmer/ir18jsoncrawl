import csv
import json

metal = ["death", "opeth", "slayer", "venom", "black sabbath"]
hiphop = ["kanye west", "eminem", "ying yang twins", "notorious b.i.g.", "snoop dogg"]
pop = ["taylor swift", "rihanna", "michael jackson", "madonna", "justin bieber"]
rock = ["rolling stones", "pink floyd", "thin lizzy", "jimi hendrix", "iron maiden"]
country = ["johnny cash", "willie nelson", "hank williams", "dolly parton", "george jones"]

genres = [metal, hiphop, pop, rock, country]
fnames = ["metal.json", "hiphop.json", "pop.json", "rock.json", "country.json"]

def generateJSON(genre, fileName, outdir = "./json/"):
	tot = 0
	with open(outdir + fileName, 'a+') as f:
		for artist in genre:
			count = 0
			with open("songdata.csv") as csvfile:
				reader = csv.DictReader(csvfile)
				for row in reader:
					rowArtist = row["artist"]
					if rowArtist.lower() == artist:
						count += 1
						f.write("\n")
						f.write("{\"index\": {}}\n")
						entry = {
							"lyrics": row["text"],
							"artist": rowArtist,
							"title": row["song"]
						}
						json.dump(entry, f)
			print("Dumped {n} songs by {a} to {fn}".format(n = count, a = artist, fn = fileName))
			tot += count
		f.write("\n")
	return tot


def main():
	tot = 0
	for i in range(len(genres)):
		cur = generateJSON(genres[i], fnames[i])
		print("Genre #{i} had {n} songs dumped.".format(i = i+1, n = cur))
		tot += cur
	print("Dumped a total of ", tot, " songs.")

main()