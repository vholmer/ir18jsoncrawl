"""
AZLyrics Scraper
Irene Chen (github/irenetrampoline)
Feb 27, 2016

Created primarily to scrape Taylor Swift lyrics off of AZ Lyrics.

To run, use the following command:
python scrape.py

The script will then create a html/ folder and a data/ folder for easy caching.
Note that AZ Lyrics is very sensitive to scrapers, so this script will pause for
extremely cautious lengths of time.
"""

import bs4
import urllib
from urllib.parse import urlparse
from urllib.parse import urljoin
from urllib.request import urlopen
import gzip
import json
from os import makedirs
from bs4 import Comment
from os.path import join, isdir, exists
import time
import numpy as np
from random import randint
from hashlib import sha1

TEST = ['https://www.azlyrics.com/k/keiynanlonsdale.html', 'https://www.azlyrics.com/s/sanni.html']

REGGAE = ['https://www.azlyrics.com/b/bobmarley.html', 'https://www.azlyrics.com/j/jimmycliff.html', 'https://www.azlyrics.com/d/damianmarley.html', 'https://www.azlyrics.com/p/petertosh.html', 'https://www.azlyrics.com/b/bujubanton.html']

HIPHOP = ['https://www.azlyrics.com/w/west.html', 'https://www.azlyrics.com/j/jayz.html', 'https://www.azlyrics.com/k/kendricklamar.html', 'https://www.azlyrics.com/e/eminem.html', 'https://www.azlyrics.com/19/2pac.html']

ROCK = ['https://www.azlyrics.com/r/rollingstones.html', 'https://www.azlyrics.com/l/ledzeppelin.html', 'https://www.azlyrics.com/p/pinkfloyd.html', 'https://www.azlyrics.com/a/acdc.html', 'https://www.azlyrics.com/b/blacksabbath.html']

POP = ['https://www.azlyrics.com/t/taylorswift.html', 'https://www.azlyrics.com/r/rihanna.html', 'https://www.azlyrics.com/j/jackson.html', 'https://www.azlyrics.com/m/madonna.html', 'https://www.azlyrics.com/j/justinbieber.html']

DEATH = ['https://www.azlyrics.com/d/death.html', 'https://www.azlyrics.com/m/morbidangel.html', 'https://www.azlyrics.com/c/cannibalcorpse.html', 'https://www.azlyrics.com/o/opeth.html', 'https://www.azlyrics.com/a/atthegates.html' ]

CACHE_DIR = 'html/'

class LyricsWalker(object):
	"""
	Class for lyric gathering. Currently for AZ Lyrics, but can be adapted into
	base case with varying scraping functions.
	"""
	@classmethod
	def convert(cls, data):
		if isinstance(data, bytes):  return data.decode('utf-8')
		if isinstance(data, dict):   return dict(map(cls.convert, data.items()))
		if isinstance(data, tuple):  return map(cls.convert, data)
		return data
	
	@classmethod
	def get_song_info(cls, url):
		"""
		Grabs lyrics from URL and return plain text lyrics.
		Lyrics are located at /html/body/div[3]/div/div[2]/div[6]
		"""
		if cls.load_local(url):
			soup = bs4.BeautifulSoup(cls.load_local(url), "html.parser")
		else:
			soup, html = cls.get_soup_from_url(url, html=True)
			cls.store_local(url, html)

		try:
			album_info = soup.find('div', {'class': 'album-panel'})
			if album_info:
				album_text = album_info.text.strip()
				album = album_text.split('"')[1]#.encode('utf-8', 'ignore')
				year = album_text.split('(')[1][:-1]#.encode('utf-8', 'ignore')
			else:
				album, year = None, None
			title = soup.title.text.split(' - ')[-1]#.encode('utf-8', 'ignore')
			lyrics = cls.get_lyrics(soup)
		except:
			print('Error parsing: %s' % url)
			return None

		entry_info = {
			'lyrics': lyrics,
			'album': album,
			'year': year,
			'title': title
		}

		entry_info = cls.utf8_encoder(entry_info)

		titleList = [x for x in title if x != " " and x != "/"]
		title_str = ""
		for i in titleList:
			title_str += str(i)
		if album is not None:
			output_file_dir = 'data/%s_%s/' % (album, year)
		else:
			output_file_dir = 'data/noalbum/'

		if not isdir(output_file_dir):
			makedirs(output_file_dir)

		output_filename = join(output_file_dir, '%s.json' % title_str)
		f = open(output_filename, 'w+')
		entry_info = cls.convert(entry_info)
		json.dump(entry_info, f, sort_keys=1, indent=2)
		f.close()

		print('Parsed %s' % title)
		return entry_info

	@classmethod
	def get_lyrics(cls, soup):
		"""
		Method for extracting lyrics from BeautifulSoup. Relies on the fact that
		lyrics are very long instead of relying on consistent formatting from AZ Lyrics.
		"""
		divs = soup.findAll("div")
		for div in divs:
			if div.get("class") == None:
				lyrics = div.text
		#print(all_text[10])
		# py.test.set_trace()
		return lyrics.strip()#.encode('utf-8', 'ignore')

	@classmethod
	def get_soup_from_url(cls, url, html=False):
		urlobject = urlopen(url)
		time.sleep(randint(5, 30)) # Never change 5 or 30
		print('Read %s' % url)
		full_html = urlobject.read()

		if html:
			return bs4.BeautifulSoup(full_html, "html.parser"), full_html
		return bs4.BeautifulSoup(full_html, "html.parser")

	@classmethod
	def url_to_filename(cls, url):
		"""
		Make a URL into a file name, using SHA1 hashes.
		"""
		hash_file = sha1(url.encode('utf-8')).hexdigest() + '.html'
		return join(CACHE_DIR, hash_file)

	@classmethod
	def store_local(cls, url, content):
		if not isdir(CACHE_DIR):
			makedirs(CACHE_DIR)

		local_path = cls.url_to_filename(url)
		with open(local_path, 'w+') as f:
			f.write(str(content))
			print('Stored locally %s' % url)

	@classmethod
	def load_local(cls, url):
		local_path = cls.url_to_filename(url)
		if not exists(local_path):
			return None
		with open(local_path, 'r+') as f:
			print('Loaded locally: %s' % url)
			return f.read()

	@classmethod
	def utf8_encoder(cls, data):
		utf8_encode = lambda x: str(x)#.encode('utf-8') if x is not None else None
		return dict(map(utf8_encode, pair) for pair in data.items())

	@classmethod
	def walk_homepage(cls, home_url, fileName, output_dir=''):
		soup = cls.get_soup_from_url(home_url)
		# py.test.set_trace()
		song_lst = soup.find(id='listAlbum').findAll(target='_blank')

		had_previous = False
		filename = join(output_dir, fileName)
		with open(filename, 'a+') as f:
			for song in song_lst:
				#f.write('\n' if had_previous else '')
				f.write("\n")
				had_previous = True
				song_url = urljoin(home_url, song.get('href'))
				f.write('{"index": {}}\n')
				json.dump(cls.get_song_info(song_url), f)

			#f.write(']')
		print("Wrote all songs to json!")

if __name__ == '__main__':
	for artist in REGGAE:
		LyricsWalker.walk_homepage(home_url=artist, fileName = "reggae.json")
	for artist in HIPHOP:
		LyricsWalker.walk_homepage(home_url=artist, fileName = "hiphop.json")
	for artist in ROCK:
		LyricsWalker.walk_homepage(home_url=artist, fileName = "rock.json")
	for artist in POP:
		LyricsWalker.walk_homepage(home_url=artist, fileName = "pop.json")
	for artist in DEATH:
		LyricsWalker.walk_homepage(home_url=artist, fileName = "death.json")